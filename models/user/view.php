<?php

if ($nom != "") {
	$id = ' value="'.htmlentities($id).'"';
	$nom = ' value="'.htmlentities($nom).'"';
	$prenom = ' value="'.htmlentities($prenom).'"';
	$adresse = ' value="'.htmlentities($adresse).'"';
	$ville = ' value="'.htmlentities($ville).'"';
	$CP = ' value="'.htmlentities($CP).'"';
	$date_naissance = ' value="'.htmlentities($date_naissance).'"';
	$ville_naissance = ' value="'.htmlentities($ville_naissance).'"';
	$num_secu = ' value="'.htmlentities($num_secu).'"';
	$id_medecin = htmlentities($id_medecin);
}


$sortie = '<div class="my-3 text-center">
        <h1 class="my-1">';

if ($id == "") $sortie .= "Création d'un ";
else $sortie .= "Modification d'un ";

$sortie .= 'patient</h1>
        <hr class="border-main border-width-3" style="width: 15%">
    </div>
    <form class="container" style="margin-bottom: 8em;" action="" method="POST">
		<div class="form-row">
';
$sortie .= selectCivilite($civilite);
$sortie .= '			<div class="md-form mt-3 col-md-5">
				Nom
				<input required type="text" class="form-control" maxlength="50" placeholder="Nom"'.$nom.' name="nom">
			</div>
			<div class="md-form mt-3 col-md-5">
				Prénom
	    		<input required type="text" class="form-control" maxlength="50" placeholder="Prénom"'.$prenom.' name="prenom">
			</div>
		</div>
		<div class="md-form mt-3">
			Adresse
			<input required type="text" class="form-control" maxlength="100" placeholder="Adresse"'.$adresse.' name="adresse">
		</div>
		<div class="form-row">
			<div class="md-form mt-3 col">
				Ville
		    	<input required type="text" class="form-control" maxlength="50" placeholder="Ville"'.$ville.' name="ville">
			</div>
			<div class="md-form mt-3 col">
				Code Postal
		    	<input required type="number" class="form-control" min="10000" max="99999" placeholder="Code Postal"'.$CP.' name="CP">
			</div>
		</div>
		<div class="form-row">
			<div class="md-form mt-3 col">
				Ville de naissance
				<input required type="text" class="form-control" maxlength="50" placeholder="Ville de naissance"'.$ville_naissance.' name="ville_naissance">
			</div>
			<div class="md-form mt-3 col">
				Date de naissance
				<input required type="date" class="form-control"'.$date_naissance.' name="date_naissance">
			</div>
		</div>
		<div class="form-row">
			<div class="md-form mt-3 col-md-2">
	    		Medecin
	    		';
$sortie .= selectPersonn("Medecin", True, $id_medecin);

$sortie .= '			</div>
			<div class="md-form mt-3 col-md-10">
				Numéro de sécurité sociale
		    	<input required type="number" class="form-control" min="100000000000000" max="999999999999999" placeholder="Numéro de sécurité sociale" '.$num_secu.' name="num_secu">
			</div>
		</div>
';

if ($dupliqNumSec == True) $sortie .= '		<div class="alert alert-danger text-center">Ce numéro de sécurité sociale est déja renseigné pour un autre patient</div>
';

$sortie .= '		<div class="md-form mt-3">
	    	<button class="btn btn-primary" type="submit"'.$id.' name="id">';

if ($id == "") $sortie .= "Création d'un";
else $sortie .= "Modification d'un";

$sortie .= ' patient</button>
		</div>
    </form>';

return $sortie;
?>
