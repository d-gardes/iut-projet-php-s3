<?php

$id = ""; $civilite = ""; $nom = ""; $prenom = ""; $adresse = ""; $ville = ""; $CP = ""; $date_naissance = ""; $ville_naissance = ""; $num_secu = ""; $id_medecin = ""; $dupliqNumSec = False; // create vars for view.php

if (isset($_POST['num_secu'])) {
	$checkDoublonNumSecu = $GLOBALS['bdd']->prepare("SELECT Id_Patient FROM Patient WHERE numSecu = ? ");
	$checkDoublonNumSecu->execute([$_POST['num_secu']]);
	$checkDoublonNumSecu = $checkDoublonNumSecu->fetchAll();
	/*var_dump($checkDoublonNumSecu);
	var_dump(isset($checkDoublonNumSecu[0]['Id_Patient']));
	exit();*/
}

switch ($_GET['action']) {

	case 'new':
		if (isset($_POST['nom'])) {
			$req = $GLOBALS['bdd']->prepare("INSERT INTO `Patient` (`civilite`, `nom`, `prenom`, `adresse`, `CP`, `ville`, `date_naissance`, `lieu_naissance`, `numSecu`, `Id_Medecin`) VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ?, ?) ");
			if (!isset($checkDoublonNumSecu[0]['Id_Patient'])) {
				if ($_POST['Id_Medecin'] == 'null') $_POST['Id_Medecin'] = null;
				$req->execute(array(
					$_POST['civilite'],
					$_POST['nom'],
					$_POST['prenom'],
					$_POST['adresse'],
					$_POST['CP'],
					$_POST['ville'],
					$_POST['date_naissance'],
					$_POST['ville_naissance'],
					$_POST['num_secu'],
					$_POST['Id_Medecin']));
				header('Location: /patients/');
			} else {
				$civilite = $_POST['civilite'];
				$nom = $_POST['nom'];
				$prenom = $_POST['prenom'];
				$adresse = $_POST['adresse'];
				$ville = $_POST['ville'];
				$CP = $_POST['CP'];
				$date_naissance = $_POST['date_naissance'];
				$ville_naissance = $_POST['ville_naissance'];
				$num_secu = $_POST['num_secu'];
				$id_medecin = $_POST['Id_Medecin'];
				$dupliqNumSec = True;
				return include("view.php");
			}
		} else return include("view.php");
		break;

	case 'edit':
		if (isset($_POST['id']) && (!isset($checkDoublonNumSecu[0]['Id_Patient']) || $checkDoublonNumSecu[0]['Id_Patient'] == $_POST['id'] )) {
			$req = $GLOBALS['bdd']->prepare("UPDATE `Patient` SET
				civilite = ? ,
				nom = ? ,
				prenom = ? ,
				adresse = ? ,
				CP = ? ,
				ville = ? ,
				date_naissance = ? ,
				lieu_naissance = ? ,
				numSecu = ?,
				Id_Medecin = ?
				WHERE id_Patient = ? ");
			if ($_POST['Id_Medecin'] == 'null') $_POST['Id_Medecin'] = null;
			$req->execute(array(
				$_POST['civilite'],
				$_POST['nom'],
				$_POST['prenom'],
				$_POST['adresse'],
				$_POST['CP'],
				$_POST['ville'],
				$_POST['date_naissance'],
				$_POST['ville_naissance'],
				$_POST['num_secu'],
				$_POST['Id_Medecin'],
				$_POST['id']));
			header('Location: /patients/');
		} else if (isset($_GET['id'])){
			$req = $GLOBALS['bdd']->prepare("SELECT * FROM Patient WHERE id_Patient = ?");
			$req->execute([$_GET['id']]);
			$rep = $req->fetch();
				$id = $rep['Id_Patient'];
				$civilite = $rep['civilite'];
				$nom = $rep['nom'];
				$prenom = $rep['prenom'];
				$adresse = $rep['adresse'];
				$ville = $rep['ville'];
				$CP = $rep['CP'];
				$date_naissance = $rep['date_naissance'];
				$ville_naissance = $rep['lieu_naissance'];
				$num_secu = $rep['numSecu'];
				$id_medecin = $rep['Id_Medecin'];
			$req->closeCursor();
			if (!isset($checkDoublonNumSecu[0]['Id_Patient']) || $checkDoublonNumSecu[0]['Id_Patient'] != $_POST['id']) $dupliqNumSec = True;
			return include("view.php");
		} else header('Location: '.$_SERVER['HTTP_REFERER']); // error no GET and no POST
		break;

	case 'delete':
		if (isset($_GET['id'])) {
			$req = $GLOBALS['bdd']->prepare("DELETE FROM Patient WHERE Id_Patient = ?");
			$reqRDV = $GLOBALS['bdd']->prepare("DELETE FROM RDV WHERE Id_Patient = ?");
			$req->execute([$_GET['id']]);
			$reqRDV->execute([$_GET['id']]);
			header('Location: '.$_SERVER['HTTP_REFERER']); // redirect to previous page
		} else header('Location: '.$_SERVER['HTTP_REFERER']); // redirect to previous page
		break;

	default:
		header('Location: '.$_SERVER['HTTP_REFERER']); // redirect to previous page
		break;
}

?>
