<?php

if ($nom != "") {
	$id = ' value="'.htmlentities($id).'"';
	$nom = ' value="'.htmlentities($nom).'"';
	$prenom = ' value="'.htmlentities($prenom).'"';
}


$sortie = '<div class="my-3 text-center">
        <h1 class="my-1">';

if ($id == "") $sortie .= "Création d'un";
else $sortie .= "Modification du";

$sortie .= ' médecin</h1>
        <hr class="border-main border-width-3" style="width: 15%">
    </div>
    <form class="container" style="margin-bottom: 8em;" action="" method="POST">
		<div class="form-row">
';
$sortie .= selectCivilite($civilite);
$sortie .= '			<div class="md-form mt-3 col-md-5">
				Nom
				<input required type="text" class="form-control" maxlength="50" placeholder="Nom"'.$nom.' name="nom">
			</div>
			<div class="md-form mt-3 col-md-5">
				Prénom
	    		<input required type="text" class="form-control" maxlength="50" placeholder="Prénom"'.$prenom.' name="prenom">
			</div>
		</div>
		<div class="md-form mt-3">
	    	<button class="btn btn-primary" type="submit"'.$id.' name="id">';

if ($id == "") $sortie .= "Création d'un";
else $sortie .= "Modification du";

$sortie .= ' médecin</button>
		</div>
    </form>';

return $sortie;
?>
