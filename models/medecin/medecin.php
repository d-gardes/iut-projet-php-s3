<?php

$id = ""; $civilite = ""; $nom = ""; $prenom = ""; // create vars for view.php

switch ($_GET['action']) {

	case 'new':
		if (isset($_POST['nom'])) {
			$req = $GLOBALS['bdd']->prepare("INSERT INTO `Medecin` (`civilite`, `nom`, `prenom`) VALUES ( ? , ? , ? )");
			$req->execute(array(
				$_POST['civilite'],
				$_POST['nom'],
				$_POST['prenom']));
			header('Location: /medecins/');
		} else return include("view.php");
		break;

	case 'edit':
		if (isset($_POST['id'])) {
			$req = $GLOBALS['bdd']->prepare("UPDATE `Medecin` SET
				civilite = ? ,
				nom = ? ,
				prenom = ?
				WHERE Id_Medecin = ? ");
			$req->execute(array(
				$_POST['civilite'],
				$_POST['nom'],
				$_POST['prenom'],
				$_POST['id']));
			header('Location: /medecins/');
		} else if (isset($_GET['id'])){
			$req = $GLOBALS['bdd']->prepare("SELECT * FROM Medecin WHERE Id_Medecin = ?");
			$req->execute([$_GET['id']]);
			$rep = $req->fetch();
				$id = $rep['Id_Medecin'];
				$civilite = $rep['civilite'];
				$nom = $rep['nom'];
				$prenom = $rep['prenom'];
			$req->closeCursor();
			return include("view.php");
		} else header('Location: '.$_SERVER['HTTP_REFERER']); // error no GET and no POST
		break;

	case 'delete':
		if (isset($_GET['id'])) {
			$req = $GLOBALS['bdd']->prepare("DELETE FROM Medecin WHERE Id_Medecin = ?");
			$req->execute([$_GET['id']]);
			header('Location: '.$_SERVER['HTTP_REFERER']); // redirect to previous page
		} else header('Location: '.$_SERVER['HTTP_REFERER']); // redirect to previous page
		break;

	default:
		header('Location: '.$_SERVER['HTTP_REFERER']); // redirect to previous page
		break;
}

?>
