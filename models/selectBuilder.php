<?php

if (!isset($GLOBALS['bdd'])) require_once('models/BDD.php');

// Build a select for a specific DB table (optionnal third arg)
function selectPersonn(string $table, bool $isNullPossible, string $idSelected = "-1", bool $isAjaxEnabled = False) {
    if ($table != "Medecin" && $table != "Patient") return ''; // to avoid SQL fatal error
    else {

        if ($isAjaxEnabled) $sortie = '        <select onchange="ajax();"';
        else $sortie = '        <select';

        $sortie .= ' id="select_'.$table.'" class="custom-select" name="Id_'.$table."\">\n";
        if ($isNullPossible) $sortie .= '            <option value="null">Tous '.strtolower($table)."s</option>\n";

        $rep = $GLOBALS['bdd']->query("SELECT Id_".$table.", civilite, nom FROM ".$table." ORDER BY nom");

        foreach ($rep as $row) {
            $sortie .= '            <option value="'.$row['Id_'.$table].'"';
            if ($row['Id_'.$table] == $idSelected) $sortie .= " selected";
            $sortie .= '>'.$row['civilite']." ".$row['nom']."</option>\n";
        }
        $sortie .= '        </select>';

        return $sortie;
    }
}

function selectCivilite(string $civilSelected) {

$sortie = '    <div class="md-form mt-3 col-md-2">
        Civilité
        <select required class="custom-select" name="civilite">
            <optgroup label="Civilité">
';
    $typeC = ["Mr", "Mme", "Mlle"];
    for ($i=0; $i < 3; $i++) {
    $sortie .= '		        	    <option value="'.$typeC[$i].'"';
    if ($civilSelected == $typeC[$i]) $sortie .= " selected";
    $sortie .= '>'.$typeC[$i]."</option>\n";
    }

    $sortie .= '			        </optgroup>
        </select>
    </div>
';
    return $sortie;

}

?>
