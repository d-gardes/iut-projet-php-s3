<?php

if ($heureRDV != "") {
	$Id_Medecin = htmlentities($Id_Medecin);
	$Id_Patient = htmlentities($Id_Patient);
	$dateRDV = ' value="'.htmlentities($dateRDV).'"';
	$heureRDV = ' value="'.htmlentities($heureRDV).'"';
	$duree = ' value="'.htmlentities($duree).'"';
}


$sortie = '<div class="my-3 text-center">
        <h1 class="my-1">';

if ($duree == "") $sortie .= "Création d'une";
else $sortie .= "Modification de la";

$sortie .= ' consultation</h1>
        <hr class="border-main border-width-3" style="width: 15%">
    </div>
    <form class="container" style="margin-bottom: 8em;" action="" method="POST">
		<div class="form-row">
			<div class="md-form col-md-6">
				Médecin
				<div id="div_select_medecin">
';
$sortie .= include('models/ajax.php');

$sortie .= '			</div>
        </div>
        <div class="col-md-6">
            Patient
';
$sortie .= selectPersonn("Patient", False, $Id_Patient, True);




$sortie .= '		</div>
	</div>
	<div class="form-row">
';

$sortie .= '			<div class="md-form mt-3 col-md-4">
				Date
				<input required type="date" class="form-control" name="dateRDV"'.$dateRDV.'>
			</div>
			<div class="md-form mt-3 col-md-4">
				Heure
	    		<input required type="time" class="form-control" name="heureRDV"'.$heureRDV.'>
			</div>
			<div class="md-form mt-3 col-md-4">
				Durée
	    		<input required type="time" class="form-control" name="duree"'.$duree.'>
			</div>
		</div>
';

if ($enRDV == True) $sortie .= '		<div class="alert alert-danger text-center">Le médecin sélectionné est déjà en Rendez-Vous à cette heure</div>
';

$sortie .= '		<div class="md-form mt-3 row justify-content-center">
	    	<button class="btn btn-primary" type="submit" name="id">';

if ($duree == "") $sortie .= "Création d'une";
else $sortie .= "Modification d'une";

$sortie .= ' consultation</button>
		</div>
    </form>
	<script>
		function ajax() {
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
			  	if (this.readyState == 4 && this.status == 200) {
			    	document.getElementById("div_select_medecin").innerHTML =
			    	this.responseText;
				}
			};
			var select = document.getElementById("select_Patient");
			var req = "/ajax/" + select[select.selectedIndex].value;
			xhttp.open("GET", req, true);
			xhttp.send();
		}
	</script>
';

return $sortie;
?>
