<?php

function testChevauchementRDV($Id_Medecin, $dateRDV, $heureRDV, $duree) {
	$test = "SELECT heureRDV, duree FROM RDV WHERE Id_Medecin = 1 AND dateRDV = '2019-12-29' ORDER BY heureRDV DESC LIMIT 1";
	$req = $GLOBALS['bdd']->prepare("SELECT heureRDV, duree FROM RDV WHERE Id_Medecin = ? AND dateRDV = ? ORDER BY heureRDV DESC LIMIT 1");
	$req->execute([$Id_Medecin, $dateRDV]);
	$rep = $req->fetchAll();
	$dernierRDV = strtotime($dateRDV) + strtotime($rep[0]['heureRDV'], 0) + strtotime($rep[0]['duree'], 0);
	$nouvRDV = strtotime($dateRDV) + strtotime($heureRDV, 0) + strtotime($duree, 0);

	return ($dernierRDV <= $nouvRDV);
}


$Id_Medecin = ""; $Id_Patient = ""; $dateRDV = ""; $heureRDV = ""; $duree = "00:30"; $enRDV = ""; // create vars for view.php

switch ($_GET['action']) {

	case 'new':
		if (isset($_POST['Id_Medecin'])) {
			$req = $GLOBALS['bdd']->prepare("INSERT INTO RDV (`Id_Medecin`, `dateRDV`, `heureRDV`, `duree`, `Id_Patient`) VALUES ( ? , ? , ? , ? , ? )");
			if (testChevauchementRDV($_POST['Id_Medecin'], $_POST['dateRDV'], $_POST['heureRDV'], $_POST['duree'])) {
				$req->execute(array(
					$_POST['Id_Medecin'],
					$_POST['dateRDV'],
	                $_POST['heureRDV'],
	                $_POST['duree'],
					$_POST['Id_Patient']));
				header('Location: /consultations/');
			} else {
				$Id_Medecin = $_POST['Id_Medecin'];
				$dateRDV = $_POST['dateRDV'];
				$heureRDV = $_POST['heureRDV'];
				$duree = $_POST['duree'];
				$Id_Patient = $_POST['Id_Patient'];
				$enRDV = True;
				return include("view.php");
			}

		} else return include("view.php");
		break;

	case 'edit':
		if (isset($_POST['Id_Medecin']) && isset($_POST['dateRDV']) && isset($_POST['heureRDV']) &&
			isset($_GET['Id_Medecin'])  && isset($_GET['dateRDV'])  && isset($_GET['heureRDV'])) {
			$req = $GLOBALS['bdd']->prepare("UPDATE RDV SET
												Id_Medecin = ? ,
												dateRDV = ? ,
												heureRDV = ? ,
												duree = ? ,
												Id_Patient = ?
												WHERE Id_Medecin = ?
												  AND dateRDV = ?
												  AND heureRDV = ? ");

			if (testChevauchementRDV($_POST['Id_Medecin'], $_POST['dateRDV'], $_POST['heureRDV'], $_POST['duree'])) {
				$req->execute(array(
					$_POST['Id_Medecin'],
					$_POST['dateRDV'],
					$_POST['heureRDV'],
					$_POST['duree'],
					$_POST['Id_Patient'],
					$_GET['Id_Medecin'],
					$_GET['dateRDV'],
					$_GET['heureRDV']));
				header('Location: /consultations/');
			} else $enRDV = True; goto jmp;; // goto pas terrible mais évite un refacto de tout le model !

		} else jmp: if (isset($_GET['Id_Medecin']) && isset($_GET['dateRDV']) && isset($_GET['heureRDV'])){
			$req = $GLOBALS['bdd']->prepare("SELECT * FROM RDV WHERE Id_Medecin = ? AND dateRDV = ? AND heureRDV = ?");
			$req->execute([$_GET['Id_Medecin'], $_GET['dateRDV'], $_GET['heureRDV']]);
			$rep = $req->fetch();
				$Id_Medecin = $rep['Id_Medecin'];
				$Id_Patient = $rep['Id_Patient'];
				$dateRDV = $rep['dateRDV'];
				$heureRDV = substr($rep['heureRDV'], 0, strlen($rep['heureRDV'])-3);
				$duree = substr($rep['duree'], 0, strlen($rep['duree'])-3);
			$req->closeCursor();
			return include("view.php");
		} else header('Location: '.$_SERVER['HTTP_REFERER']); // error no GET and no POST
		break;

	case 'delete':
		if (isset($_GET['Id_Medecin']) && isset($_GET['dateRDV']) && isset($_GET['heureRDV'])) {
			$req = $GLOBALS['bdd']->prepare("DELETE FROM RDV WHERE Id_Medecin = ? AND dateRDV = ? AND heureRDV = TIME( ? )");
			$ret = $req->execute([$_GET['Id_Medecin'], $_GET['dateRDV'], $_GET['heureRDV']]);
			header('Location: '.$_SERVER['HTTP_REFERER']); // redirect to previous page
		} else header('Location: '.$_SERVER['HTTP_REFERER']); // redirect to previous page
		break;

	default:
		header('Location: '.$_SERVER['HTTP_REFERER']); // redirect to previous page
		break;
}

?>
