<?php

require('BDD.php');
if (!function_exists('selectPersonn')) include('selectBuilder.php');

$req = $GLOBALS['bdd']->prepare("SELECT Id_Medecin FROM Patient WHERE Id_Patient = ?");

if (!isset($Id_Medecin)) {
    $req->execute([$_GET['id']]);
    $rep = $req->fetchAll();
    if ($rep[0]['Id_Medecin'] != null) echo selectPersonn("Medecin", True, $rep[0]['Id_Medecin']);
    else echo selectPersonn("Medecin", False);
    exit();
}

if ($Id_Medecin != null) {
    $req->execute([$Id_Medecin]);
    $rep = $req->fetchAll();
    if ($rep[0]['Id_Medecin'] != null) return selectPersonn("Medecin", True, $rep[0]['Id_Medecin']);
}
return selectPersonn("Medecin", False);
?>
