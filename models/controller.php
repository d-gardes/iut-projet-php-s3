<?php

require('models/selectBuilder.php');

if (isset($_GET['type']) && isset($_GET['action'])) {
	switch ($_GET['type']) {

		case 'user':
			return include("user/user.php");
			break;

		case 'medecin':
			return include("medecin/medecin.php");
			break;

		case 'consultation':
			return include("consultation/consultation.php");
			break;

		default:
			header('Location: /');
			exit();
			break;
	}
} else header('Location: /'); exit(); // si paramètres manquants on va sur la page d'acceuil

?>
