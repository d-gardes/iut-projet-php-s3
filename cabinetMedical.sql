-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  sam. 21 déc. 2019 à 17:34
-- Version du serveur :  5.7.28-0ubuntu0.18.04.4
-- Version de PHP :  7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `cabinetMedical`
--

-- --------------------------------------------------------

--
-- Structure de la table `Medecin`
--

CREATE TABLE `Medecin` (
  `Id_Medecin` int(11) NOT NULL,
  `civilite` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `Medecin`
--

INSERT INTO `Medecin` (`Id_Medecin`, `civilite`, `nom`, `prenom`) VALUES
(1, 'Mr', 'Dupont', 'Albert'),
(2, 'Mme', 'Dutunnel', 'Gertrude');

-- --------------------------------------------------------

--
-- Structure de la table `Patient`
--

CREATE TABLE `Patient` (
  `Id_Patient` int(11) NOT NULL,
  `civilite` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `adresse` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `CP` char(5) COLLATE utf8_unicode_ci NOT NULL,
  `ville` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_naissance` date NOT NULL,
  `lieu_naissance` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `numSecu` char(15) COLLATE utf8_unicode_ci NOT NULL,
  `Id_Medecin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `Patient`
--

INSERT INTO `Patient` (`Id_Patient`, `civilite`, `nom`, `prenom`, `adresse`, `CP`, `ville`, `date_naissance`, `lieu_naissance`, `numSecu`, `Id_Medecin`) VALUES
(1, 'Mlle', 'Lachaise', 'Valérie', '123 rue de la rue', '31000', 'Toulouse', '2001-01-01', 'Toulouse', '201013100104584', NULL),
(2, 'Mr', 'Laporte', 'Henri', '123456 rue de la rue', '31000', 'Toulouse', '2001-01-01', 'Toulouse', '999999999999999', NULL),
(3, 'Mr', 'Hibulaire', 'Pat', '123 rue des embrouilles', '31300', 'Toulouse', '2002-02-02', 'Paris', '123456789123456', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `RDV`
--

CREATE TABLE `RDV` (
  `Id_Medecin` int(11) NOT NULL,
  `dateRDV` date NOT NULL,
  `Heure_RDV` time NOT NULL,
  `Duree` time NOT NULL DEFAULT '00:30:00',
  `Id_Patient` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Medecin`
--
ALTER TABLE `Medecin`
  ADD PRIMARY KEY (`Id_Medecin`);

--
-- Index pour la table `Patient`
--
ALTER TABLE `Patient`
  ADD PRIMARY KEY (`Id_Patient`),
  ADD KEY `Id_Medecin` (`Id_Medecin`);

--
-- Index pour la table `RDV`
--
ALTER TABLE `RDV`
  ADD PRIMARY KEY (`Id_Medecin`,`dateRDV`,`Heure_RDV`),
  ADD KEY `Id_Patient` (`Id_Patient`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `Medecin`
--
ALTER TABLE `Medecin`
  MODIFY `Id_Medecin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `Patient`
--
ALTER TABLE `Patient`
  MODIFY `Id_Patient` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `Patient`
--
ALTER TABLE `Patient`
  ADD CONSTRAINT `Patient_ibfk_1` FOREIGN KEY (`Id_Medecin`) REFERENCES `Medecin` (`Id_Medecin`);

--
-- Contraintes pour la table `RDV`
--
ALTER TABLE `RDV`
  ADD CONSTRAINT `RDV_ibfk_1` FOREIGN KEY (`Id_Medecin`) REFERENCES `Medecin` (`Id_Medecin`),
  ADD CONSTRAINT `RDV_ibfk_2` FOREIGN KEY (`Id_Patient`) REFERENCES `Patient` (`Id_Patient`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
