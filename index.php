<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
date_default_timezone_set("Europe/Paris");
session_start();

require_once('models/BDD.php');


//Initialisation de la page html
$titre="Cabinet Médical";
$coquille_vide = include("views/header_footer/header_footer.php");
$path="";
$navbar="";

// Check login et récupération de l'action à effectuer
if (isset($_SESSION["logged"]) && $_SESSION["logged"] == TRUE && $_SESSION['REMOTE_ADDR'] = $_SERVER['REMOTE_ADDR']) {
    if (isset($_GET['a'])) $action=$_GET['a'];
    else $action="";
    $navbar = include("views/header_footer/navbar.php");
    $coquille_vide=preg_replace('/{#CSS}/', '<link rel="stylesheet" href="/navbar/navbar.css">
    {#CSS}',$coquille_vide); // CSS linked changed by rewrite rules (see .htaccess)

} else $action="login";

switch ($action) {

    case "login":
        $path="views/login/";
        break;

	// Si aucune action n'est envoyée dans l'URL on affiche l'accueil (seulement quand connecté)
	case "":
		$path="views/acceuil/";
		break;

    case "patients":
        $path="views/patients/";
        break;

    case "medecins":
        $path="views/medecins/";
        break;

	case "edit":
		$path="models/";
		break;

    case "consultations":
    	$path="views/consultations/";
    	break;

    case "stats":
    	$path="views/stats/";
    	break;

    case "logout": // Déconnexion demandée
        session_destroy();
        header('Location: /');
        exit();
        break;

	default:  //Si page n'existe pas
    header('Location: /');
    exit();
	break;
}

/*if ($coquille_vide == "") { //Si rien à afficher (Erreur serveur)

	$contenu=file("vue/500.html");
}*/

$coquille_vide=preg_replace('/{#NAVBAR}/', $navbar, $coquille_vide);
$coquille_vide=preg_replace('/{#CONTENU}/', include($path."controller.php"), $coquille_vide);
$coquille_vide=preg_replace('/{#TITRE}/', $titre, $coquille_vide);

if (file_exists($path."style.css")) $coquille_vide=preg_replace('/{#CSS}/', "<style>". file_get_contents($path."style.css") ."</style>",$coquille_vide);
else $coquille_vide=preg_replace('/{#CSS}/', "",$coquille_vide);

//Affichage final de la page puisque la coquille n'est plus vide !!

print $coquille_vide;
?>
