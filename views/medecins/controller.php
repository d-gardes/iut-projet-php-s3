<?php

$sortie = '<div class="my-3 text-center">
    <h1 class="my-1">Medecins</h1>
    <hr class="border-main border-width-3" style="width: 15%">
</div>
<ul class="list-inline list-unstyled my-4 text-center">
'; // Sortie stocke le code qui sera renvoyé

$req = $GLOBALS['bdd']->prepare("SELECT * FROM Medecin WHERE nom LIKE :lettreNom");

$tableauMedecins='<a class="btn btn-primary" style="margin-left: 1.5%;" href="/edit/medecin/new/">Nouveau médecin</a>
<div class="container-fuild">
'; // $tableauMedecins stocke le code HTML qui génère le tableau des medecins (ajouté à $sortie avant l'envoi)
$compteurDeLettres=0;

for ($i=65; $i < 90; $i++) { // from A to Z

    $req->execute([ 'lettreNom' => chr($i)."%"]);
    $medecins = $req->fetchAll();

    if (count($medecins) > 0) {
        $compteurDeLettres+=1;
        $sortie .= '    <li class="list-inline-item mx-2">
        <a href="#'.chr($i).'" class="text-black">
            <h2 class="my-0 font-weight-bold">'.chr($i).'</h2>
        </a>
    </li>
';

        $tableauMedecins .= '	<div class="row  bg-gray-'.strval(($compteurDeLettres % 2)+1).'00  py-3">
        <div class="col-mg-4 initiales text-right text-black-50">
            <h1 id="'.chr($i).'" class="my-0 letterTitle">'.chr($i).'</h1>
        </div>
        <div class="col border-left border-width-2 border-black-50">
        ';
        foreach ($medecins as $personne) {
            $tableauMedecins .= '   <h4 class="my-2 cursor-pointer" data-toggle="collapse" data-target="#user'.htmlentities($personne['Id_Medecin']).'">
                '.htmlentities(strtoupper($personne['nom'])). " " .htmlentities($personne['prenom']).'
            </h4>
            <div class="collapse my-2 table-responsive" id="user'.htmlentities($personne['Id_Medecin']).'">
                <table class="table">
                    <thead class="thead-dark"><tr><th>Civilité</th><th>Nom</th><th>Prénom</th><th></th><th></th></tr></thead>
                    <tbody>
                        <tr>
                            <td>'.htmlentities($personne['civilite']).'</td>
                            <td>'.htmlentities($personne['nom']).'</td>
                            <td>'.htmlentities($personne['prenom']).'</td>
                            <td class="col-md-1"><a class="btn btn-primary" href="/edit/medecin/edit/'.$personne['Id_Medecin'].'">Modifier</a></td>
                            <td class="col-md-1"><a class="btn btn-danger" href="/edit/medecin/delete/'.$personne['Id_Medecin'].'">Supprimer</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        ';
        }
        $tableauMedecins .= "</div>
	</div>\n";
    }
}

return $sortie."</ul>\n" . $tableauMedecins."</div>";
?>
