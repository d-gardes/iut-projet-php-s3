<?php

return '<div class="centered">
<h2><center>Que souhaitez-vous faire ?</center></h2>
    <br>
    <center>
        <div class="gr1">
            <a href="/consultations/"  class="btn btn-primary btn-lg">Consultations</a>
            <a href="/stats/"  class="btn btn-primary btn-lg">Statistiques</a>
            <a href="/patients/"   class="btn btn-primary btn-lg">Patients</a>
            <a href="/medecins/" class="btn btn-primary btn-lg">Médecins</a>
        </div>
    </center>
</div>
';

?>
