<?php

$sortie = '<div class="my-3 text-center">
    <h1 class="my-1">Patients</h1>
    <hr class="border-main border-width-3" style="width: 15%">
</div>
<ul class="list-inline list-unstyled my-4 text-center">
'; // Sortie stocke le code qui sera renvoyé

$req = $GLOBALS['bdd']->prepare("SELECT * FROM Patient WHERE nom LIKE :lettreNom");
$reqMedecin = $GLOBALS['bdd']->prepare("SELECT civilite, nom FROM Medecin WHERE Id_Medecin = ? ");

$tableauPatients='<a class="btn btn-primary" style="margin-left: 1.5%;" href="/edit/user/new/">Nouveau patient</a>
<div class="container-fuild">
'; // $tableauPatients stocke le code HTML qui génère le tableau des patients (ajouté à $sortie avant l'envoi)
$compteurDeLettres=0;

for ($i=65; $i < 90; $i++) { // from A to Z

    $req->execute([ 'lettreNom' => chr($i)."%"]);
    $patients = $req->fetchAll();

    if (count($patients) > 0) {
        $compteurDeLettres+=1;
        $sortie .= '    <li class="list-inline-item mx-2">
        <a href="#'.chr($i).'" class="text-black">
            <h2 class="my-0 font-weight-bold">'.chr($i).'</h2>
        </a>
    </li>
';

        $tableauPatients .= '	<div class="row  bg-gray-'.strval(($compteurDeLettres % 2)+1).'00  py-3">
        <div class="col-mg-4 initiales text-right text-black-50">
            <h1 id="'.chr($i).'" class="my-0 letterTitle">'.chr($i).'</h1>
        </div>
        <div class="col border-left border-width-2 border-black-50">
        ';
        foreach ($patients as $personne) {
            $tableauPatients .= '   <h4 class="my-2 cursor-pointer" data-toggle="collapse" data-target="#user'.htmlentities($personne['Id_Patient']).'">
                '.htmlentities(strtoupper($personne['nom'])). " " .htmlentities($personne['prenom']).'
            </h4>
            <div class="collapse my-2 table-responsive" id="user'.htmlentities($personne['Id_Patient']).'">
                <table class="table">
                    <thead class="thead-dark"><tr><th>Civilité</th><th>Nom</th><th>Prénom</th><th>Adresse</th><th>Naissance</th><th>Numéro de sécurité sociale</th><th>Médecin</th><th></th><th></th></tr></thead>
                    <tbody>
                        <tr>
                            <td>'.htmlentities($personne['civilite']).'</td>
                            <td>'.htmlentities($personne['nom']).'</td>
                            <td>'.htmlentities($personne['prenom']).'</td>
                            <td>'.htmlentities($personne['adresse']).'<br>'.htmlentities($personne['ville']).' '.htmlentities($personne['CP']).'</td>
                            <td>'. date('d/m/Y', strtotime(htmlentities($personne['date_naissance']))).'<br>'.htmlentities($personne['lieu_naissance']).'</td>
                            <td>'.htmlentities($personne['numSecu']).'</td>
                            <td>';
            if ($personne['Id_Medecin'] != "") {
                $reqMedecin->execute([$personne['Id_Medecin']]);
                $medecin = $reqMedecin->fetch();
                $reqMedecin->closeCursor();
                $tableauPatients .= $medecin['civilite']." ".$medecin['nom'];
            }
            $tableauPatients .= '</td>
                            <td class="col-md-1"><a class="btn btn-primary" href="/edit/user/edit/'.$personne['Id_Patient'].'">Modifier</a></td>
                            <td class="col-md-1"><a class="btn btn-danger" href="/edit/user/delete/'.$personne['Id_Patient'].'">Supprimer</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        ';
        }
        $tableauPatients .= "</div>
	</div>\n";
    }
}

return $sortie."</ul>\n" . $tableauPatients."</div>";
?>
