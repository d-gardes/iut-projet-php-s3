<?php

$nbPatients = (($GLOBALS['bdd']->query("SELECT count(*) nbPatients FROM Patient"))->fetch())['nbPatients'];
$hommes = (($GLOBALS['bdd']->query("SELECT TIMESTAMPDIFF(YEAR, date_naissance, CURDATE()) age FROM Patient WHERE civilite = 'Mr'"))->fetchAll());
$femmes = (($GLOBALS['bdd']->query("SELECT TIMESTAMPDIFF(YEAR, date_naissance, CURDATE()) age FROM Patient WHERE civilite != 'Mr'"))->fetchAll());

$nbHm25 = 0; $nbH2550 = 0; $nbHp50 = 0; $nbFm25 = 0; $nbF2550 = 0; $nbFp50 = 0;

foreach ($hommes as $homme) {
    if ($homme['age']<25) $nbHm25 ++;
    else if ($homme['age']>25) $nbH2550 ++;
    else $nbHp50 ++;
}

foreach ($femmes as $femme) {
    if ($femme['age']<25) $nbFm25 ++;
    else if ($femme['age']>25) $nbF2550 ++;
    else $nbFp50 ++;
}

$sortie = '<div class="my-3 text-center">
    <h1 class="my-1">Statistiques du cabinet</h1>
    <hr class="border-main border-width-3" style="width: 15%">
</div>
<div class="container">
    <center>
        <table class="table table-striped table-bordered my-5">
            <thead class="thead-dark">
                <tr>
                    <th>Tranche d\'âge</th>
                    <th>Nb Hommes</th>
                    <th>Nb Femmes</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Moins de 25 ans</th>
                    <td>'.$nbHm25.' - '. $nbHm25 / $nbPatients * 100 .'%</td>
                    <td>'.$nbFm25.' - '. $nbFm25 / $nbPatients * 100 .'%</td>
                </tr>
                <tr>
                    <th>Entre 25 et 50 ans</th>
                    <td>'.$nbH2550.' - '. $nbH2550 / $nbPatients * 100 .'%</td>
                    <td>'.$nbF2550.' - '. $nbF2550 / $nbPatients * 100 .'%</td>
                </tr>
                <tr>
                    <th>Plus de 50 ans</th>
                    <td>'.$nbHp50.' - '. $nbHp50 / $nbPatients * 100 .'%</td>
                    <td>'.$nbFp50.' - '. $nbFp50 / $nbPatients * 100 .'%</td>
                </tr>
            </tbody>
        </table>
        <table class="table table-striped table-bordered mb-5">
            <thead class=thead-dark>
                <tr>
                    <th>Médecins</th>
                    <th>Nombre d\'heures effectuées</th>
                </tr>
            </thead>
            <tbody>
';
$medecins = ($GLOBALS['bdd']->query("SELECT * FROM Medecin"))->fetchAll();
foreach ($medecins as $medecin) {
    $nbHeures = (($GLOBALS['bdd']->query("SELECT TIME_FORMAT(sum(Duree), '%H h %i m') nbHeures FROM RDV WHERE Id_Medecin =".$medecin['Id_Medecin']))->fetch())['nbHeures']; // ugly but in one line
    if ($nbHeures == NULL) $nbHeures = "00 h";
    $sortie .= '
                    <tr>
                        <td>'.htmlentities($medecin['civilite']).' '.htmlentities($medecin['nom']).'</td>
                        <td>'.$nbHeures.'</td>
                    </tr>
    ';
}
$sortie .= '            </tbody>
        </table>
    </center>
</div>
';
return $sortie;
?>
