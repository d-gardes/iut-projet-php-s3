<?php
require('models/selectBuilder.php');

$reqStr = "
    SELECT  m.civilite mCivilite, m.nom mNom,
            p.civilite pCivilite, p.nom pNom,
            DATE_FORMAT(r.dateRDV, '%d %M %Y') date,
            TIME_FORMAT(r.heureRDV, '%Hh %i') heure,
            r.duree,
            r.Id_Medecin,
            r.dateRDV,
            r.heureRDV
    FROM Medecin m, Patient p, RDV r
    WHERE m.Id_Medecin = r.Id_Medecin
      AND p.Id_Patient = r.Id_Patient";

$medSelected = "-1"; $patSelected = "-1";

if (isset($_POST['Id_Medecin']) && isset($_POST['Id_Patient'])) {
    $reqArgs =array();

    if ($_POST['Id_Patient'] != 'null') {
        $reqStr .= " AND r.Id_Patient = :patient ";
        $reqArgs['patient'] = $_POST['Id_Patient'];
        $patSelected = $_POST['Id_Patient'];
    }

    if ($_POST['Id_Medecin'] != 'null') {
        $reqStr .= " AND r.Id_Medecin = :medecin ";
        $reqArgs['medecin'] = $_POST['Id_Medecin'];
        $medSelected = $_POST['Id_Medecin'];
    }

    $reqStr .= " ORDER BY r.dateRDV DESC, r.heureRDV ASC";
    $req = $GLOBALS['bdd']->prepare($reqStr);
    $req->execute($reqArgs);

} else { $req = $GLOBALS['bdd']->prepare($reqStr); $req->execute(); }

$sortie = '<div class="my-3 text-center">
    <h1 class="my-1">Consultations</h1>
    <hr class="border-main border-width-3" style="width: 15%">
</div>
<div class="container my-5">
    <div class="row justify-content-between">
        <a class="btn btn-primary" href="/edit/consultation/new/">Nouvelle consultation</a>
        {#TODAY}
    </div>
    <div class="row justify-content-center">
        <h5>Filtrer les consultations</h5>
    </div>
    <form class="row my-3" action="" method="POST">
        <div class="md-form col-md-5">
            Médecin
        ';
$sortie .= selectPersonn("Medecin", True, $medSelected);

$sortie .= '        </div>
        <div class="col-md-5">
            Patient
        ';
$sortie .= selectPersonn("Patient", True, $patSelected);

$sortie .= '        </div>
        <div class="col-md-2">
            <button class="btn btn-primary form-control mt-4" type="submit">Filtrer</button>
        </div>
    </form>
    <div class="my-5">
';

$today = (($GLOBALS['bdd']->query("SELECT DATE_FORMAT(CURDATE(), '%d %M %Y') FROM dual"))->fetchAll()[0][0]); // in case of the SQL and the PHP server haven't the same hour configured
$rep = $req->fetchAll();
$datePrecedente = "";

foreach ($rep as $rdv) {
    if ($datePrecedente != $rdv['date']) {
        if ($datePrecedente != "") {
            $sortie .= '                    </tbody>
                </table>
            </div>
        </div>
';
        }
        $i=0;
        $datePrecedente = $rdv['date'];
        if ($rdv['date'] == $today) { $sortie .= '        <div id="today" class="row">'; $btnToday = True; } // set flag to true
        else $sortie .= '        <div class="row">
';

        $sortie .= '            <h4 class="my-2">'.$rdv['date'].'</h4>
                <div class="my-2 table-responsive">
                    <table class="table">
                        <thead class="thead-dark"><tr><th>Heure</th><th>Medecin</th><th>Patient</th><th>Durée</th><th></th><th></th></tr></thead>
                        <tbody>
';
    } // end if

    $sortie .= '                        <tr class="bg-gray-'.strval(($i % 2)+1).'00">
                            <td>'.$rdv['heure'].'</td>
                            <td>'.$rdv['mCivilite'].' '.$rdv['mNom'].'</td>
                            <td>'.$rdv['pCivilite'].' '.$rdv['pNom'].'</td>
                            <td>'.substr($rdv['duree'], 0, strlen($rdv['duree'])-3).'</td>
                            <td class="col-md-2"><a class="btn btn-primary" href="/edit/consultation/edit/'.$rdv['Id_Medecin'].'/'.$rdv['dateRDV'].'/'.$rdv['heureRDV'].'/">Modifier</a></td>
                            <td class="col-md-2"><a class="btn btn-danger" href="/edit/consultation/delete/'.$rdv['Id_Medecin'].'/'.$rdv['dateRDV'].'/'.$rdv['heureRDV'].'/">Supprimer</a></td>
                        </tr>
';
    $i++;
} // end foreach

$sortie .= '                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
';

if (isset($btnToday) && $btnToday == True) $btnToday ='<a class="btn btn-primary" href="#today">Allez aux réservations du jour</a>';
else $btnToday = "";

$sortie=preg_replace('/{#TODAY}/', $btnToday, $sortie);

return $sortie;
?>
