<?php

function returnLogin(bool $mdpIncorect) {
    $sortie = '<div class="login-clean">
        <form method="post" action="/login/">
            <h2 class="sr-only">Login Form</h2>
            <div class="illustration">
                <svg style="width:  50%;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                    <path fill="currentColor" d="M400 192h-32v-46.6C368 65.8 304 .2 224.4 0 144.8-.2 80 64.5 80 144v48H48c-26.5 0-48 21.5-48 48v224c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V240c0-26.5-21.5-48-48-48zm-272-48c0-52.9 43.1-96 96-96s96 43.1 96 96v48H128v-48zm272 320H48V240h352v224z"/>
                </svg>
            </div>
            <div class="form-group"><input class="form-control" type="text" name="login" placeholder="Login"></div>
            <div class="form-group"><input class="form-control" type="password" name="password" placeholder="Password"></div>';

            if ($mdpIncorect) {
                $sortie .= '
                <div class="form-group"><h6 style="color:red;">Mot de passe incorrect</h6></div>';
            }

            $sortie .= '
            <div class="form-group">
                <button class="btn btn-primary btn-block" type="submit">Log In</button>
            </div>
        </form>
    </div>
    ';
    return $sortie;
}

/**
 * Ensures an IP address is both a valid IP address and does not fall within
 * a private network range.
 * From https://stackoverflow.com/questions/1634782/what-is-the-most-accurate-way-to-retrieve-a-users-correct-ip-address-in-php
 */
function validate_ip($ip) {
    if (filter_var($ip, FILTER_VALIDATE_IP,
                        FILTER_FLAG_IPV4 |
                        FILTER_FLAG_IPV6 ) === false) {
        return false;
    } else return true;
}

if (isset($_POST['login']) && isset($_POST['password'])) { // check login

    if ($_POST['login'] == "root" && $_POST['password'] == '$iutinfo' && validate_ip($_SERVER['REMOTE_ADDR'])) {
        $_SESSION['logged'] = TRUE;
        $_SESSION['REMOTE_ADDR'] = $_SERVER['REMOTE_ADDR'];

        if (isset($_GET['a'])) header('Location: '.$_SERVER['HTTP_REFERER']); // renvoie à la page demandée avant connexion
        else header('Location: /');

        exit();
    } else {
        return returnLogin(TRUE);
    }

} else if (isset($_SESSION["logged"])) { // Just in case redirect to root

    header('Location: /');
    exit();

} else { // print login page

    return returnLogin(FALSE);
}

?>
