<?php

return '<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>{#TITRE}</title>
    <link rel="stylesheet" href="/bootstrap/bootstrap.min.css">
    {#CSS}
    <link rel="stylesheet" href="/navbar/header_footer.css">
</head>

<body>

{#NAVBAR}

{#CONTENU}

<footer class="page-footer font-small">
  <div class="footer-copyright text-center py-3">
    IUT Informatique Paul Sabatier S3D 2019 © GARDES - FOUCHOU-LAPEYRADE
  </div>

</footer>
<script src="/bootstrap/jquery.min.js"></script>
<script src="/bootstrap/bootstrap.min.js"></script>
</body>
</html>
';
 ?>
