<?php

return '<nav class="navbar navbar-light navbar-expand-lg navigation-clean">
    <div class="container">
        <a class="navbar-brand" href="/">Cabinet médical</a>
        <button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1">
            <span class="sr-only">Dérouler le menu</span>
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navcol-1">
            <div class="nav navbar-nav ml-auto">
                <div class="btn-group">
                    <a href="/consultations/" class="btn btn-primary">Consultations</a>
                    <a href="/medecins/" class="btn btn-primary">Médecins</a>
                    <a href="/patients/" class="btn btn-primary">Patients</a>
                    <a href="/stats/" class="btn btn-primary">Statistiques</a>
                </div>
                <a href="/logout/" class="btn btn-danger">Déconnexion</a>
            </div>
        </div>
    </div>
</nav>
';
 ?>
